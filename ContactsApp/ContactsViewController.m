//
//  ContactsViewController.m
//  ContactsApp
//
//  Created by Gary Davis on 2/22/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

#import "ContactsViewController.h"

@interface ContactsViewController ()

@end

@implementation ContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor grayColor];
    
    firstName = [UITextField new];
    firstName.translatesAutoresizingMaskIntoConstraints = NO;
    firstName.placeholder = @"First Name";
    firstName.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:firstName];
    
    lastName = [UITextField new];
    lastName.placeholder = @"Last Name";
    lastName.backgroundColor = [UIColor whiteColor];
    lastName.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:lastName];
    
    phoneNumber = [UITextField new];
    phoneNumber.placeholder = @"Phone Number";
    phoneNumber.backgroundColor = [UIColor whiteColor];
    phoneNumber.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:phoneNumber];
    
    address = [UITextField new];
    address.placeholder = @"Address";
    address.backgroundColor = [UIColor whiteColor];
    address.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:address];
    
    email = [UITextField new];
    email.placeholder = @"eMail Address";
    email.backgroundColor = [UIColor whiteColor];
    email.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:email];
    
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(firstName, lastName, phoneNumber, address, email);
    
    NSDictionary *metrics = @{@"margin":[NSNumber numberWithInteger:20], @"topmargin":[NSNumber numberWithInteger:70]};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[firstName]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[lastName]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[phoneNumber]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[address]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[email]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topmargin-[firstName(40)]-10-[lastName(40)]-10-[phoneNumber(40)]-10-[address(40)]-10-[email(40)]" options:0 metrics:metrics views:viewsDictionary]];
    
    UIBarButtonItem* saveButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveButtonTouched:)];
    
    UIBarButtonItem* shareButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionButtonTouched:)];
    
    UIBarButtonItem* messageButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target: self action:@selector(messageButtonTouched:)];
    
    UIBarButtonItem* deleteButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteButtonTouched:)];
    
    self.navigationItem.rightBarButtonItems = @[saveButton, deleteButton, shareButton, messageButton];
    
    if (self.contacts) {
        firstName.text = [self.contacts valueForKey:@"firstName"];
        lastName.text = [self.contacts valueForKey:@"lastName"];
        phoneNumber.text = [self.contacts valueForKey:@"phoneNumber"];
        address.text = [self.contacts valueForKey:@"address"];
        email.text = [self.contacts valueForKey:@"email"];
    }
}

-(void)messageButtonTouched:(id)sender{
    MFMessageComposeViewController *messSend = [[MFMessageComposeViewController alloc] init];
    messSend.messageComposeDelegate = self;
    
    [messSend setRecipients:[NSArray arrayWithObjects:phoneNumber.text, nil]];
    [messSend setBody:@"Sending a message"];
    
    [self presentViewController:messSend animated:YES completion:^{
        
    }];
}

-(void)actionButtonTouched:(id)sender {
    
    MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
    mailCont.mailComposeDelegate = self;
    
    [mailCont setSubject:@"Contact Person"];
    [mailCont setToRecipients:[NSArray arrayWithObject:@"email"]];
    NSString* message = [NSString stringWithFormat:@"%@\n%@\n%@\n%@", firstName.text, lastName.text, phoneNumber.text, address.text];
    [mailCont setMessageBody:message isHTML:NO];
    
    [self presentViewController:mailCont animated:YES completion:^{
        
    }];
}

-(void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
                                                                                 

// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)deleteButtonTouched:(id)sender
{
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    NSError *error = nil;
    [context deleteObject:self.contacts];
    if (![context save:&error]) {
        NSLog(@"Can't Delete! %@ %@", error, [error localizedDescription]);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)saveButtonTouched:(id)sender {
    AppDelegate* appD = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = appD.persistentContainer.viewContext;
    
    if (self.contacts) {
        [self.contacts setValue:firstName.text forKey:@"firstName"];
        [self.contacts setValue:lastName.text forKey:@"lastName"];
        [self.contacts setValue:phoneNumber.text forKey:@"notes"];
        [self.contacts setValue:address.text forKey:@"address"];
        [self.contacts setValue:email.text forKey:@"email"];
    } else {
        //Create a new managed object
        NSManagedObject *newContact = [NSEntityDescription insertNewObjectForEntityForName:@"Contacts" inManagedObjectContext:context];
        [newContact setValue:firstName.text forKey:@"firstName"];
        [newContact setValue:lastName.text forKey:@"lastName"];
        [newContact setValue:phoneNumber.text forKey:@"phoneNumber"];
        [newContact setValue:address.text forKey:@"address"];
        [newContact setValue:email.text forKey:@"email"];
        
    }
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
