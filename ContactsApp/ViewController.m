//
//  ViewController.m
//  ContactsApp
//
//  Created by Gary Davis on 2/22/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    contactsTable = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    contactsTable.translatesAutoresizingMaskIntoConstraints = NO;
    contactsTable.delegate = self;
    contactsTable.dataSource = self;
    [self.view addSubview:contactsTable];
    
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(contactsTable);
    
    NSDictionary *metrics = @{};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[contactsTable]|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[contactsTable]|" options:0 metrics:metrics views:viewsDictionary]];
    
    UIBarButtonItem* createButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(createButtonTouched:)];
    self.navigationItem.rightBarButtonItem = createButton;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

-(void)loadData {
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Contacts"];
    NSArray* array = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    contactsArray = [array mutableCopy];
    [contactsTable reloadData];
}

-(void)createButtonTouched:(id)sender {
    ContactsViewController* dvc = [ContactsViewController new];
    [self.navigationController pushViewController:dvc animated:YES];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSManagedObject* object = contactsArray[indexPath.row];
    
    cell.textLabel.text = [object valueForKey:@"firstName"];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return contactsArray.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ContactsViewController* dvc = [ContactsViewController new];
    dvc.contacts = contactsArray[indexPath.row];
    [self.navigationController pushViewController:dvc animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
