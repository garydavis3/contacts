//
//  ViewController.h
//  ContactsApp
//
//  Created by Gary Davis on 2/22/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactsViewController.h"

@interface ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    UITableView* contactsTable;
    NSMutableArray* contactsArray;
}


@end

