//
//  ContactsViewController.h
//  ContactsApp
//
//  Created by Gary Davis on 2/22/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <Messages/Messages.h>
#import <MessageUI/MessageUI.h>

@interface ContactsViewController : UIViewController<MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>
{
    UITextField* firstName;
    UITextField* lastName;
    UITextField* phoneNumber;
    UITextField* address;
    UITextField* email;
}
@property (nonatomic, strong) NSManagedObject* contacts;
@end
